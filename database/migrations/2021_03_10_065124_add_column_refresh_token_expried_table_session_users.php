<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRefreshTokenExpriedTableSessionUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('session_users', function (Blueprint $table) {
            //
            $table->datetime('refresh_token_expried');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('session_users', function (Blueprint $table) {
            $table->dropColumn('refresh_token_expried');
        });
    }
}
