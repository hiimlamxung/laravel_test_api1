<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use Auth;
use App\SessionUser;
use App\Services\ProductService;
use App\Http\Resources\Product as ProductResource;
use App\Http\Requests\StoreProductRequest;

class ProductController extends Controller
{
    //
	protected $product_service;
	public function __construct(ProductService $product_service)
	{
		$this->product_service = $product_service;
	}
	public function index(Request $request)
	{
		try{
			$limit = $request->get('limit') ?? 10;//k có request limit thì cho bằng 10.
			$orderBy = array();
			if($request->get('column') && $request->get('sort')){
				$orderBy['column'] 	= $request->get('column');
				$orderBy['sort'] 	= $request->get('sort');
			}
			$product = $this->product_service->getAll($limit, $orderBy);
			return response()->json([
				'code'		=>	200,
				'message'	=> 'get data success',
				'data'		=>	$product->items('data'),
				'meta'		=>[
					'total'			=>	$product->total(),
					'perPage'		=>	$product->perPage(),
					'currentPage'	=>	$product->currentPage(),

				]
			],200);
		}catch(\Exception $e){
			return response()->json([
				'code'		=>	500,
				'message'	=> $e->getMessage(),
			],500);
		}
	}

	public function store(StoreProductRequest $request)
	{
		try{
			$product = Product::create($request->all());
			return response()->json([
				'code'		=>	201,
				'data'		=>	$product,
			],201);
		}catch(\Exception $e){
			return response()->json([
				'code'		=>	500,
				'message'	=> $e->getMessage() . 'Line:'. $e->getLine(),
			],500);
		}
	}
	public function show(Product $id)
	{
		 return new ProductResource($id);
	}

	public function update(\App\Http\Requests\UpdateProductRequest $request, Product $id)
	{
		try{
			$product = $id->update(['name' => $request->name]);
			return response()->json([
				'code'		=>	200,
				'message'	=> 'update success',
				'data'		=>	Product::find($id),
			],200);
		}catch(\Exception $e){
			return response()->json([
				'code'		=>	500,
				'message'	=> $e->getMessage() . 'Line:'. $e->getLine(),
			],500);
		}
	}

	public function destroy(Product $id){
		try{
			$product = $id->delete();
			return response()->json([
				'code'		=>	200,
				'message'	=> 'delete success',
			],200);
		}catch(\Exception $e){
			return response()->json([
				'code'		=>	500,
				'message'	=> $e->getMessage() . 'Line:'. $e->getLine(),
			],500);
		}
	}
}
