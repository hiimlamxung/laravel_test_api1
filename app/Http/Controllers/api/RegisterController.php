<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Validator;
use App\Http\Requests\RegisterUserRequest;

class RegisterController extends Controller
{
    //
    public function register(RegisterUserRequest $request)
    {
        try{
            $user_create = User::create([
                'name'      =>  $request->name,
                'email'     =>  $request->email,
                'password'  =>  bcrypt($request->password),
            ]);

            return response()->json([
                'code'  =>  201,
                'data'  =>  $user_create,
            ],201);
        }catch(\Exception $e){
            return response()->json([
                'code'      => 500,
                'message'   => $e->getMessage().' Line:'. $e->getLine(),
            ],500);
        }
    }
}
