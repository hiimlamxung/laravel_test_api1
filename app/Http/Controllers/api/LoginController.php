<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SessionUser;
use Auth;
use Illuminate\Support\Str;
use Validator;
use App\Http\Requests\PostLoginRequest;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(PostLoginRequest $request)
    {
        try{
            $checkTokenExist = SessionUser::where('user_id',Auth::user()->id)->first(); //Check xem user này đã được tạo token chưa
            if(empty($checkTokenExist)){
                $userSession = SessionUser::create([
                    'token'                 =>  Str::random(40),
                    'refresh_token'         =>  Str::random(40),
                    'token_expried'         =>  date('Y-m-d H:i:s', strtotime('+30 day')),
                    'refresh_token_expried' =>  date('Y-m-d H:i:s', strtotime('+360 day')),
                    'user_id'               =>  Auth::user()->id,
                ]);
            }
            else{
                $userSession = $checkTokenExist;
            }

            return response()->json([
                'code'  =>  200,
                'data'  =>  $userSession,
            ],200);
        }catch(\Exception $e){
            return response()->json([
                'code'      => 500,
                'message'   => $e->getMessage().' Line:'. $e->getLine(),
            ],500);
        }
        
    }

    public function refreshToken(Request $request)
    {
        try{
            $token              = $request->header('token');
            $checkTokenIsValid  = SessionUser::where('token',$token)->first();
            if(!empty($checkTokenIsValid)){
                if(strtotime($checkTokenIsValid->token_expried) < time()){
                    $checkTokenIsValid->update([
                        'token'                 =>  Str::random(40),
                        'refresh_token'         =>  Str::random(40),
                        'token_expried'         =>  date('Y-m-d H:i:s', strtotime('+30 day')),
                        'refresh_token_expried' =>  date('Y-m-d H:i:s', strtotime('+360 day')),
                    ]);
                }
            }else{
                return response()->json([
                    'code'  =>  401,
                    'message'  =>  'Token không hợp lệ',
                ],401);
            }
            $dataSession = SessionUser::find($checkTokenIsValid->id);
            return response()->json([
                'code'      =>  200,
                'data'      =>  $dataSession,
                'message'   =>  'refresh token success',
            ],200);
        }catch(\Exception $e){
            return response()->json([
                'code'      => 500,
                'message'   => $e->getMessage().' Line:'. $e->getLine(),
            ],500);
        }
    }

    public function deleteToken(Request $request)
    {
        try{
            $token              = $request->header('token');
            $checkTokenIsValid  = SessionUser::where('token',$token)->first();
            if(!empty($checkTokenIsValid)){
                $checkTokenIsValid->delete();
            }
            return response()->json([
                'code'      =>  204,
                'message'   =>  'delete token success',
            ],204);
        }catch(\Exception $e){
            return response()->json([
                'code'      => 500,
                'message'   => $e->getMessage().' Line:'. $e->getLine(),
            ],500);
        }
    }
}
