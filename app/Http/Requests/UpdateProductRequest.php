<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'  =>  'required|min:6',
        ];
    }

    public function messages()
    {
        return [
            'name.required'       => 'Bạn chưa nhập tên',
            'name.min'            => 'Tên tối thiểu 6 ký tự',
            'name.unique'         => 'Tên sản phẩm đã tồn tại',
        ];
    }

     //Overwrite lại hàm có sẵn của Laravel
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        // throw new HttpResponseException(response()->json($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY)); //mẫu

        //overwrite lại
        throw new HttpResponseException(response()->json([
            'code'      => 422,
            'message'   => $validator->errors()->first(),
        ], Response::HTTP_UNPROCESSABLE_ENTITY));

    }
}
