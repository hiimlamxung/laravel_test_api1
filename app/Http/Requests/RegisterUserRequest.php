<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'email'     =>  'required|email|unique:users,email',
            'password'  =>  'required|min:6',
        ];
    }

    public function messages()
    {
        return [
            'email.required'        => 'Bạn chưa nhập email',
            'email.email'           => 'Email chưa đúng định dạng',
            'password.required'     => 'Bạn chưa nhập password',
            'password.min'          => 'Password tối thiếu 6 ký tự',
        ];
    }

     //Overwrite lại hàm có sẵn của Laravel
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        // throw new HttpResponseException(response()->json($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY)); //mẫu

        //overwrite lại
        throw new HttpResponseException(response()->json([
            'code'      => 422,
            'message'   => $validator->errors()->first(),
        ], Response::HTTP_UNPROCESSABLE_ENTITY));

    }
}
