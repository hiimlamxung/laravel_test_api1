<?php

namespace App\Http\Middleware;

use Closure;
use Validator;
use Auth;

class LoginUserMiddlware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $validator = Validator::make($request->all(),[
        //     'email'     =>  'required|email',
        //     'password'  =>  'required|min:6',
        // ]);
        // if($validator->fails()){
        //     return response()->json([
        //         'code'      =>  401,
        //         'message'   =>  $validator->errors(),
        //     ],401);
        // }
        
        $dataCheckLogin = array(
            'email'     =>  $request->email,
            'password'  =>  $request->password,
        );
        if(Auth::attempt($dataCheckLogin)){
            return $next($request);
        }else{
            return response()->json([
                'code'      =>  401,
                'message'   => 'Username hoặc password không đúng',
            ],401);
        }
    }

}
