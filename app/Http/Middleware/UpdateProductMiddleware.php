<?php

namespace App\Http\Middleware;

use Closure;
use App\Product;
use Validator;

class UpdateProductMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $product = Product::find($request->id);
        if(!$product){
            return response()->json([
                'code' => 404,
                'message' => 'Product ID not found',
            ],404);
        }
        // $validator = Validator::make($request->all(),[
        //     'name'  =>  'required|min:6',
        // ]);
        // if($validator->fails()){
        //     return response()->json([
        //         'code' => 401,
        //         'message' => $validator->errors()->first(),
        //     ],401);
        // }
        return $next($request);
    }
}
