<?php

namespace App\Http\Middleware;

use Closure;
use App\SessionUser;

class ListProductMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token              = $request->header('token');
        $checkTokenIsValid  = SessionUser::where('token',$token)->first();
        if(empty($token)){
            return response()->json([
                'code'      =>  401,
                'message'   =>  'Token chưa được gửi qua headers',
            ],401);
        }else if(!$checkTokenIsValid){
            return response()->json([
                'code'      =>  403,
                'message'   =>  'Token không hợp lệ',
            ],403);
        }else{
            return $next($request);
        }
    }
}
