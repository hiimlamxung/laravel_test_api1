<?php 
namespace App\Services;

use App\Product;
/**
 * 
 */
class ProductService 
{
	
	public function getAll($limit = 10,$orderBy = [])
	{
		$query = Product::query();
		if($orderBy){
			$query->orderBy($orderBy['column'],$orderBy['sort']);
		}
		return $query->paginate($limit);
	}
}
 ?>