<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionUser extends Model
{
    //
    protected $table = 'session_users';
    protected $fillable = ['token','refresh_token','token_expried','refresh_token','user_id','refresh_token_expried'];
}
