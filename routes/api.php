<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//register
Route::post('register','api\RegisterController@register');
//login --create token
Route::post('login','api\LoginController@login')->middleware('LoginUserMiddlware');
//refresh token
Route::patch('refreshtoken','api\LoginController@refreshToken');
//delete token
Route::delete('deletetoken','api\LoginController@deleteToken');

//sản phẩm
Route::group(['prefix' => 'product','middleware' => 'ListProductMiddleware'],function(){
	Route::get('index','api\ProductController@index');
	Route::get('show/{id}','api\ProductController@show');
	Route::post('store','api\ProductController@store');
	Route::patch('update/{id}','api\ProductController@update')->middleware('UpdateProductMiddleware');
	Route::delete('delete/{id}','api\ProductController@destroy')->middleware('DeleteProductMiddleware');
});